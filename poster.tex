\input{poster/header.tex}

\title{\href{https://github.com/IBT-FMI/SAMRI}{\Large github.com/IBT-FMI/SAMRI}\\\vspace{.15em}SAMRI --- Comprehensive Small Animal MRI Workflows}
% \title{Longitudinal opto-pharmaco-fMRI of Selective\\ Serotonin Reuptake Inhibition}
\author{Horea-Ioan Ioanas$^{1}$, Markus Marks$^{2}$, Mehmet Yanik$^{2}$, Markus Rudin$^{1}$}
\institute[ETH]{
      $^{1}$Institute for Biomedical Engineering, ETH and University of Zurich\\
      $^{2}$Institute of Neuroinformatics, ETH and University of Zurich, Zurich, Switzerland}
\date{2020-08-26}

\newlength{\columnheight}
\setlength{\columnheight}{0.881\textheight}

\begin{document}

\begin{myverbbox}{\mybashcode}
user@host ~ $ SAMRI bru2bids\
  -o /var/tmp/samri_testing/bash\
  -f '{"acquisition":["EPI"]}'\
  -s '{"acquisition":["TurboRARE"]}'\
  /usr/share/samri_bindata
\end{myverbbox}

\begin{myverbbox}{\mypythoncode}
from samri.pipelines.reposit import bru2bids
bru2bids('/usr/share/samri_bindata/',
  functional_match={'acquisition':['EPI']}\closecurl,
  structural_match={'acquisition':['TurboRARE']}\closecurl,
  out_base='/var/tmp/samri_testing/pytest/',)
\end{myverbbox}

\begin{frame}
\begin{columns}
	\begin{column}{.42\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Abstract}
						\input{poster/abstract.tex}
					\end{myblock}\vfill
					\vspace{-0.5em}
					\begin{myblock}{Package Structure}
						\begin{figure}
							\captionsetup{width=.99\linewidth}
							\vspace{-1.2em}
							\includedot[width=1.0\textwidth]{data/structure}
							\vspace{-2.7em}
							\caption{
							      Graph representation of the package structure.
							}
						\end{figure}
						\begin{itemize}
						      \item \textcolor{mg}{\textbf{analysis}}: high-level analysis functions
						      \item \textcolor{mg}{\textbf{fetch}}: feature extraction from standardized projection/expression packages
						      \item \textcolor{mg}{\textbf{pipelines}}: repositing, preprocessing, and first/second level modelling workflows
						      \item \textcolor{mg}{\textbf{plotting}}: small-animal-brain optimized 2D/3D plotting functions
						      \item \textcolor{mg}{\textbf{analysis}}: high-level timecourse debugging and reporting functions
						\end{itemize}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Example Workflows}
						\begin{figure}
							\vspace{.3em}
							\captionsetup{width=.99\linewidth}
							\includegraphics[width=1.0\textwidth]{img/files}
							\vspace{-.8em}
							\caption{
							      File-detail input/output view of Bruker-to-BIDS repositing workflow \cite{aowsis}, with the Bruker data structure summarized on the left and the BIDS data structure exemplified in the right panel.
							      Processing step corresponds to the colored multi-arrow step highlighted in \cref{fig:p}.
							}
							\label{fig:b2b}
						\end{figure}
						\begin{figure}
							\vspace{-1.5em}
							\captionsetup{width=.99\linewidth}
							\includedot[width=0.9\textwidth]{data/generic}
							\vspace{-1.8em}
							\caption{
							      Atomized process overview of the “SAMRI Generic” preprocessing workflow \cite{irsabi}, with green nodes representing specific nodes which were optimized for small animal mouse brain parameters.
							      Processing step corresponds to the bold orange arrow highlight from \cref{fig:p}.
							}
							\label{fig:sg}
						\end{figure}
					\vspace{-0.4em}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Outlook}
						\begin{itemize}
							\item We are looking for \textbf{collaborators}, to expand the animal range currently covered (mice, rats) to further model animals.
							\item We are extending the test suite of the package to improve sustainability.
							\item We are working on a full stack integration testing infrastructure.
							\item We are working on a machine-learning based brain extraction solution for the preprocessing workflow.
							\item We are drafting a full length article detailing the package.
						\end{itemize}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
	\begin{column}{.59\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth} % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Methods}
						\vspace{0.2em}
					      	SAMRI's front end interfaces, which provide access to the workflows, are available in Bash and Python.
						The workflow control is implemented in Python, using construction and execution support from the nipype package.
						Established toolkits from human brain imaging, including ANTs, FSL, AFNI, SciPy, scikit-learn, and nilearn, are leveraged for atomized data processing applications (e.g. the nodes of \cref{fig:sg}).
						Plotting functions (using matplotlib and Blender) are distributed alongside the workflows, enabling operator quality control and the generation of publication-ready plots without the need of graphical editing.
					\end{myblock}\vfill
					\begin{myblock}{Data-to-Figure Overview}
						\begin{figure}
							\captionsetup{width=.99\linewidth}
							\vspace{-1.2em}
							\includedot[width=1.0\textwidth]{data/bids}
							\vspace{-2.7em}
							\caption{
							      Graph representation of data provenance across an integrated MRI raw-data to plot pipeline, from \cite{aowsis}.
							      All intermediary data processing stages are structured according to the BIDS specification.
							      The processing step highlighted with multiple colored arrows is detailed in \cref{fig:b2b}, and the edge highlighted with a bold orange arrow is detailed in \cref{fig:sg}.
							}
							\label{fig:p}
						\end{figure}
						\vspace{-.4em}
					\end{myblock}\vfill
					\begin{myblock}{Figure Examples}
						\vspace{0.1em}
						\sessionpy{pytex_subfigs(
						        [
						      	  {'script':'scripts/dw_map_ofM.py', 'conf':'poster/2asymmetric_map.conf', 'options_pre':'{.41\\textwidth}\\vspace{-.1em}',
						      		  'options_pre_caption':'\\vspace{.5em}',
						      		  'caption':'Thresholded volumetric map and dynamically generated mesh for a second-level analysis pattern.',
								  'label':'fm',
						      		  },
						      	  {'script':'scripts/dw_activity_dr.py', 'conf':'poster/2asymmetric_ses.conf', 'options_pre':'{.58\\textwidth}',
						      		  'options_pre_caption':'\\vspace{-1.em}',
								  'caption':'Region of interest measurement session timecourse, split across treatment groups, and with within-population \\SI{95}{\\percent} confidence interval error bars.',
								  'label':'ft',
						      		  },
							  {'script':'scripts/dw_distributions_neg.py', 'conf':'poster/2x1_distributions.conf', 'options_pre':'{.49\\textwidth}\\centering',
								  'figure_format':'pdf',
								  'caption':'Distribution of t-statistic values in the 10 most strongly inhibited atlas parcellation areas, for the statistc map shown in \\textbf{(\subref{fig:fm})}. \\vspace{1.5em}',
								  'label':'fn',
								  },
							  {'script':'scripts/dw_distributions_pos.py', 'conf':'poster/2x1_distributions.conf', 'options_pre':'{.49\\textwidth}\\centering',
								  'figure_format':'pdf',
								  'caption':'Distribution of t-statistic values in the 10 most strongly activated atlas parcellation areas, for the statistc map shown in \\textbf{(\subref{fig:fm})}. \\vspace{1.5em}',
								  'label':'fp',
								  },
							  {'script':'scripts/features_filtered_coronal_contour.py', 'conf':'poster/coronal.conf', 'options_pre':'{.99\\textwidth}\\centering',
								  'figure_format':'pdf',
								  'caption':'Qualitative slice-wise comparison of thresholded t-statistic maps, with a functional projections highlighted in a warm heatmap, and structural projections outlined along the same threshold.\\vspace{1.5em}',
								  'label':'fc',
								  },
						      	  ],
						        caption='
							    Figures, as can be automatically produced by SAMRI via a single function execution.
							    From neuroimaging articles detailing monoaminergic drug profiling \cite{drlfom} \\textbf{(\subref{fig:fm}, \subref{fig:ft}, \subref{fig:fn}, \subref{fig:fp})} and monoaminergic assay development \cite{opfvta} \\textbf{(\subref{fig:fc})}.							    ',
							environment='figure',
						        )}
						\vspace{-.4em}
					\end{myblock}\vfill
					\begin{myblock}{Interface Examples}
						\vspace{.2em}
			      			\begin{figure}
						\centering
							\begin{subfigure}{.36\textwidth}
								\mybashcode
								\vspace{.2em}
						      		\caption{Bash interface}
								\label{fig:bash}
							\end{subfigure}\hspace{.2em}
						\begin{subfigure}{.62\textwidth}
								\mypythoncode
								\vspace{.2em}
						      		\caption{Python interface}
								\label{fig:python}
							\end{subfigure}
							\vspace{-.2em}
					     		\caption{Bash and Python interfaces, made synchronously available via the Argh package (article fig. 3)}
				    		\end{figure}
						\vspace{-.5em}
					\end{myblock}\vfill
					\begin{myblock}{References}
                                                \vspace{-1em}
                                                \begin{multicols}{2}
                                                        \scriptsize
                                                        \bibliographystyle{ieeetr}
							\bibliography{./bib}
                                                \end{multicols}
                                                \vspace{-1em}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
\end{columns}
\end{frame}
\end{document}
