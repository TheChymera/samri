from samri.plotting.aggregate import roi_distributions
from samri.report.roi import atlasassignment

df = atlasassignment('data/dw_l2_joint/ses-ofM/acq-EPI_run-0_tstat.nii.gz',
        lateralized=False,
        value_label='t Values',
        )

df['Structure'] = df['Structure'].str.title()
df['Structure'] = df['Structure'].replace({
        'Anterior':'Ant.',
        'Cortex':'Ctx.',
        'Endopiriform Claustrum':'EC',
        'Intermediate':'Int.',
        'Medial':'Med.',
        'Nucleus':'Nc.',
        'Pars':'p.',
        'Posterior':'Post.',
        'White Matter':'WM',
        },
        regex=True,
        )
roi_distributions(df,
        max_rois=10,
        exclude_tissue_type=['CSF'],
        start=0.4,
        cmap='autumn_r',
        value_label='t Values',
        bw=.1,
        hspace=-0.5,
        xlim=[-5,8],
        )
